# ORMF

### Filter your orm by defining simple rules

```go
// Rule struct holds the information to be used in the filter.
type Rule struct {
	// The model field it will filter
	ModelField string
	// The DB expresion it will use to filter the model
	DBExpr string
	// The look up field it will search in the query
	Param string
	// The value used in the filter that has a default behaviour
	// or used by a must filter
	Value interface{}
	// Function to check if the value gotten using Param is of the right type
	CheckParam ParamFunc
}
```

---
First version
