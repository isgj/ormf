// Package popf is an orm filter for gobuffalo/pop.
package popf

import (
	"github.com/gobuffalo/pop"
	"gitlab.com/isgj/ormf"
)

// Filter struct holds all the filter rules that will be
// used in the *pop.Query or *pop.Connection.
type Filter struct {
	// FormParams holds the filter rules that will be used
	// only if ormf.Rule.CheckParam returns no error.
	FromParams ormf.Rules
	// DefaultParams holds the filter rules that will be used every time.
	// If the ormf.Rule.CheckParam returns an error the filter will use the default value (ormf.Rule.Value).
	DefaultParams ormf.Rules
	// MustFiler holds the rules used every time using ormf.Rule.Value (won't check the omrf.Rule.CheckParam).
	MustFilter ormf.Rules
}

// FilterParams is the provider from which the filter will get the values passing ormf.Rule.Param
type FilterParams interface {
	Get(string) string
}

// FilterQuery will filter the *pop.Query passed.
func (f Filter) FilterQuery(q *pop.Query, fp FilterParams) *pop.Query {
	for _, rule := range f.FromParams {
		if v, err := rule.CheckParam(fp.Get(rule.Param)); err == nil {
			q.Where(rule.GetStmt(), v)
		}
	}

	for _, rule := range f.DefaultParams {
		if v, err := rule.CheckParam(fp.Get(rule.Param)); err == nil {
			q.Where(rule.GetStmt(), v)
		} else {
			q.Where(rule.GetStmt(), rule.Value)
		}
	}

	for _, rule := range f.MustFilter {
		q.Where(rule.GetStmt(), rule.Value)
	}

	return q
}

// FilterConnection will filter the *pop.Connection passed.
func (f Filter) FilterConnection(c *pop.Connection, fp FilterParams) *pop.Query {
	return f.FilterQuery(pop.Q(c), fp)
}
