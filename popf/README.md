# POPF

## An orm filter for gobuffalo/pop

---

```go
// Filter struct holds all the filter rules that will be
// used in the *pop.Query or *pop.Connection.
type Filter struct {
	// FormParams holds the filter rules that will be used
	// only if ormf.Rule.CheckParam returns no error.
	FromParams []ormf.Rule
	// DefaultParams holds the filter rules that will be used every time.
	// If the ormf.Rule.CheckParam returns an error the filter will use the default value (ormf.Rule.Value).
	DefaultParams []ormf.Rule
	// MustFiler holds the rules used every time using ormf.Rule.Value (won't check the omrf.Rule.CheckParam).
	MustFilter []ormf.Rule
}

// FilterParams is the provider from which the filter will get the values passing ormf.Rule.Param
type FilterParams interface {
	Get(string) string
}
```

#### How to use

Say you have a model
```go
type User struct {
    Name   string // ...
    Age    int
    Active bool
}
```

Somewhere in your action package you create a popf.Filter
```go
userFilter := popf.Filter{
    FromParams: ormf.Rules{
        ormf.Rule{"age", "<", "max_age", 0, ormf.IntField},
        ormf.Rule{"lower(name)", "~", "name_like", "", ormf.StringField},
    },
    DefaultParams: ormf.Rules{
        ormf.Rule{"age", ">", "min_age", 17, ormf.IntField},
    },
    MustFilter: ormf.Rules{
        ormf.Rule{"active", "=", "", true, ormf.BoolField},
    },
}
```

Now inside an action where you want to filter the users
```go
params := c.GetParams()
users := models.Users{}
tx := c.Value("tx").(*pop.Connection)
userFilter.FilterConnection(tx, params).All(&users) // or you can use userFilter.FilterQuery(tx, params) if tx is a *pop.Query
// Now you can do something with the filtered set of users
```

So depending on the query parameters you'll get differnt set of users.

| query | used filter |
| ----- | ----------- |
| ?max\_age=50 | all users have `age < 50` |
| ?max\_age=sd | no filter used (it's **IntField**)|
| ?name\_like=is | all users have `lower(name) ~ is`|
| ?min\_age=20 | all users have `age > 20` |
| ?min\_age=sdf (or no min_age specified)| all users have `age > 17` (default value)|
| (for every query) | all users have `active = true` (it's a must filter)| 

---

First version
