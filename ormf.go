// Package ormf is the base of the orm filters.
// It contains a Rule struct and some handy type checker functions.
//
// See in the subdirectories for implementations.
package ormf

import (
	"errors"
	"strconv"
	"time"
)

// ParamFunc checks the value of the query param value.
// If error the return value won't be used in the filter.
type ParamFunc func(string) (interface{}, error)

// Rule struct holds the information to be used in the filter.
type Rule struct {
	// The model field it will filter
	ModelField string
	// The DB expresion it will use to filter the model
	DBExpr string
	// The look up field it will search in the query
	Param string
	// The value used in the filter that has a default behaviour
	// or used by a must filter
	Value interface{}
	// Function to check if the value gotten using Param is of the right type
	CheckParam ParamFunc
}

// Rules is a slice of ormf.Rule
type Rules []Rule

// GetStmt returns a string of format "ModelField DBExpr ?"
// ex:
//  	"age > ?"
func (r Rule) GetStmt() string {
	return r.ModelField + " " + r.DBExpr + " ?"
}

// IntField checks the value passed is an integer
func IntField(pm string) (interface{}, error) {
	return strconv.Atoi(pm)
}

// BoolField checks the value passed is a valid boolean
func BoolField(pm string) (interface{}, error) {
	return strconv.ParseBool(pm)
}

// TimeField returns a time checker function.
// The passed value represents the layout of the date it will parse.
// For the layout format see https://golang.org/pkg/time/#Parse
func TimeField(layout string) ParamFunc {
	return func(pm string) (interface{}, error) {
		return time.Parse(layout, pm)
	}
}

// StringField returns error only if it's passed an empty string.
func StringField(pm string) (interface{}, error) {
	if pm == "" {
		return pm, errors.New("empty string")
	}
	return pm, nil
}

// FloatField retruns a float checker function passing bitSize as second argument.
// It uses strconv.ParseFloat function https://golang.org/pkg/strconv/#ParseFloat
func FloatField(bitSize int) ParamFunc {
	return func(pm string) (interface{}, error) {
		return strconv.ParseFloat(pm, bitSize)
	}
}
